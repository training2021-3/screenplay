package steps;

import co.com.sofkau.web.questions.CompletedLogin;
import co.com.sofkau.web.tasks.Login;
import co.com.sofkau.web.tasks.OpenTheBrowser;
import co.com.sofkau.web.ui.SouceDemoLoginPage;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.is;

public class AutenticacionSteps {


    private Actor actor;
    private SouceDemoLoginPage loginPage;

    @Before
    public void setup() {
        WebDriverManager.chromedriver().setup();
        OnStage.setTheStage(new OnlineCast());
        actor = OnStage.theActorCalled("Usuario");
    }


    @Dado("un usuario en la pagina inicial de souce demo")
    public void unUsuarioEnLaPaginaInicialDeSouceDemo() {
        actor.wasAbleTo(OpenTheBrowser.on(loginPage));
    }

    @Cuando("el usuario ingresa un {string} y {string} correctos")
    public void elUsuarioIngresaUnYCorrectos(String username, String password) {
        actor.attemptsTo(Login.withCredential(username, password));
    }

    @Entonces("se puede autenticar correctamente")
    public void sePuedeAutenticarCorrectamente() {
        actor.should(
                seeThat(
                        CompletedLogin.ok(),
                        is(Matchers.equalTo("PRODUCTS"))
                )
        );
    }
}
