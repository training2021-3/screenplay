package co.com.sofkau.web.tasks;

import co.com.sofkau.web.ui.SouceDemoLoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Login implements Task {

    private String username;
    private String password;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(username).into(SouceDemoLoginPage.USER_INPUT),
                Enter.theValue(password).into(SouceDemoLoginPage.PASSWORD_INPUT),
                Click.on(SouceDemoLoginPage.LOGIN_BUTTON)
        );
    }

    public static Login withCredential(String username, String password) {
        return Tasks.instrumented(Login.class, username, password);
    }


}
