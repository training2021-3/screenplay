package co.com.sofkau.web.questions;

import co.com.sofkau.web.ui.SouceDemoHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CompletedLogin implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {
        return SouceDemoHomePage.HOME_TITLE.resolveFor(actor).getText();
    }

    public static CompletedLogin ok() {
        return new CompletedLogin();
    }
}
