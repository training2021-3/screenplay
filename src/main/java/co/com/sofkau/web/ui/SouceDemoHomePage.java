package co.com.sofkau.web.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class SouceDemoHomePage extends PageObject {

    public static final Target HOME_TITLE = Target.the("HOME_TITLE").locatedBy("//*[@class='title']");
}
